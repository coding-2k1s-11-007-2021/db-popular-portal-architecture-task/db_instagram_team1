CREATE INDEX index_Usersname ON Users USING hash(Usersname)  --найти пользователя
CREATE INDEX new_posts_by_user ON Posts (ID)  --найти все посты от пользователя
CREATE INDEX new_comments_by_user ON Comments (ID)  --найти все комменты от пользователя
CREATE INDEX new_likes_by_user ON Likes (ID)  --найти все лайки от пользователя (Вкладка "понравилось")


--view post

select

    Time,
    Text,
    User.Username,
    Content_Link

from Posts

left join User on User.ID = Posts.ID

--view list of followers and person who you follow

select

    Follower_ID,
    Following_ID,

from Followers

left join User on User.ID = Followers.Following_ID
left join User on User.ID = Followers.Follower_ID

--view comment under post

select

    Time,
    Text,
    User.Username,


from Comments

left join Posts on Posts.ID = Comments.Post_ID
left join User on User.ID = Comments.ID

where Post_ID = Posts.ID and Posts.ID = Comments.ID

--view user profile

select

    Username,
    count(Followers.Follower_ID),
    count(Followers.Following_ID),
    Name

from User

left join Followers on User.ID = Followers.Following_ID
left join Followers on User.ID = Followers.Follower_ID
