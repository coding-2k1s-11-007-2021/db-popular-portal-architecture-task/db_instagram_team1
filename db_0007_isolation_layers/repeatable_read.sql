--уровень read commited

begin transaction;

set transaction isolation level repeatable read
select item_count, price
from Items
where ID = '1338'

commit transaction;

--то же самое что и с уровнем ниже, но

--(этот уровень подходит для случая когда товар добавленный в корзину закрепляется за пользователем на какое - то время,
-- юзер гарантированно купит то, что добавил в корзину)

--пример с возможными ошибками

-- сначала мы смотрим на первом терминале всю таблицу:
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SELECT * FROM Orders;


-- на втором же терминале мы добавляем строку и обновляем некоторые строки:
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;
INSERT INTO Orders
VALUES ( 1118, 4561, 2021-11-11, 3000, 1367);
UPDATE Orders SET Cost_of_order = cost - 1000
WHERE Date = '2021-11-18';
END;


-- на первом терминале проверяем изменения, и никаких изменений нет, из за того что данные взяты на
-- момент начала первого запроса транзакции
SELECT * FROM Orders;
-- после завершаем транзакцию
END;

-- смотрим опять изменения и теперь они уже есть, но пока мы находились
-- в процессе выполнения первой транзакции, все эти изменения не были ей
SELECT * FROM Orders;
