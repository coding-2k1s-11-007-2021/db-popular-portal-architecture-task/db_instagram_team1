begin transaction;

CREATE TABLE Users
(
    ID int,
    Username varchar(30) UNIQUE,
    Name varchar(30) NOT NULL,
    Email varchar(50) NOT NULL,
    Hashed_Password varchar(30) NOT NULL,
    Secret_Key varchar(30) NOT NULL,

    PRIMARY KEY (ID)
);

CREATE TABLE Followers
(
    ID int,
    Follower_ID int,
    Following_ID int,

    PRIMARY KEY (ID),
    CONSTRAINT id_of_follower FOREIGN KEY (Follower_ID) REFERENCES Users (ID) ON DELETE CASCADE,
    CONSTRAINT id_of_following_person FOREIGN KEY (Following_ID) REFERENCES Users (ID) ON DELETE CASCADE
);

CREATE TABLE Posts
(
    ID int,
    Time timestamp NOT NULL,
    Text varchar(150),
    User_ID int NOT NULL,
    Content_Link varchar(150) not null,

    PRIMARY KEY (ID),
    CONSTRAINT id_of_post_author FOREIGN KEY (User_ID) REFERENCES Users (ID) ON DELETE CASCADE
);

CREATE TABLE Comments
(
    ID int,
    Time timestamp NOT NULL,
    Text varchar(150) NOT NULL,
    User_ID int NOT NULL,
    Post_ID int NOT NULL,

    PRIMARY KEY (ID),
    CONSTRAINT id_of_comment_author FOREIGN KEY (User_ID) REFERENCES Users (ID) ON DELETE CASCADE,
    CONSTRAINT id_of_commented_post FOREIGN KEY (Post_ID) REFERENCES Posts (ID) ON DELETE SET NULL
);

CREATE TABLE Likes
(
    ID int,
    User_ID int NOT NULL,
    Comment_ID int NOT NULL,
    Post_ID int NOT NULL,

    PRIMARY KEY (ID),
    CONSTRAINT ID FOREIGN KEY (User_ID) REFERENCES Users (ID) ON DELETE CASCADE,
    CONSTRAINT id_of_liked_post FOREIGN KEY (Post_ID) REFERENCES Posts (ID) ON DELETE SET NULL,
    CONSTRAINT id_of_liked_comment FOREIGN KEY (Comment_ID) REFERENCES Comments (ID) ON DELETE SET NULL
);


CREATE VIEW list_posts_with_authors AS  --список постов с дополнительной информацией об авторе
(
SELECT Posts.*,   --выводит все поля из таблицы Posts
       Users.Username,
       Users.Name

FROM Posts
LEFT JOIN Users ON Users.ID = Posts.ID
);

CREATE VIEW likes_by_user AS  --посмотреть посты которые лайкнул пользователь
(
SELECT  Content_Link,
        Text

FROM Posts

LEFT JOIN Likes ON Likes.Post_ID = Posts.ID
LEFT JOIN Users ON Users.ID = Likes.ID

WHERE Username = 'alex_228'
);


CREATE MATERIALIZED VIEW user_list_with_likes_ct AS  --список пользователей с кол-вом оставленных лайков
(
with posts_by_users as (
    			select Users.ID
                FROM Users

                LEFT JOIN Posts ON Users.ID = Posts.User_ID
                LEFT JOIN Likes ON Likes.Post_ID = Posts.ID

                GROUP BY Users.ID, Posts.ID
                        )

select Username,
        count(posts_by_users)
from Users
left join posts_by_users on Users.ID = posts_by_users.ID
group by username
);

commit transaction;